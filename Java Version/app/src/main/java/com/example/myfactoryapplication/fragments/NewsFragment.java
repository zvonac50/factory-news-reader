package com.example.myfactoryapplication.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.myfactoryapplication.R;
import com.example.myfactoryapplication.helpers.CustomAlerts;
import com.squareup.picasso.Picasso;

public class NewsFragment extends Fragment {


    public static NewsFragment create(Bundle bundle) {
        NewsFragment f = new NewsFragment();
        f.setArguments(bundle);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news, container, false);

        displayNews(view);
        return view;
    }

    private void displayNews(View v) {
        assert getArguments() != null;
        String title = getArguments().getString("title");
        String desc = getArguments().getString("desc");
        String imgUrl = getArguments().getString("imgUrl");

        if (title == null || desc == null || imgUrl == null) {
            CustomAlerts.showError(getContext(), "unknown");
            return;
        }

        TextView tv_title = v.findViewById(R.id.news_fragment_title);
        tv_title.setText(title);
        TextView tv_desc = v.findViewById(R.id.news_fragment_desc);
        tv_desc.setText(desc);
        ImageView img_news = v.findViewById(R.id.news_fragment_img);
        Picasso.get().load(imgUrl).into(img_news);
    }
}
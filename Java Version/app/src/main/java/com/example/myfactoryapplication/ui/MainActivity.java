package com.example.myfactoryapplication.ui;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myfactoryapplication.R;
import com.example.myfactoryapplication.adapters.NewsAdapter;
import com.example.myfactoryapplication.helpers.CustomAlerts;
import com.example.myfactoryapplication.helpers.LocalStoreManager;
import com.example.myfactoryapplication.interfaces.GetNewsDataService;
import com.example.myfactoryapplication.models.News;
import com.example.myfactoryapplication.models.NewsList;
import com.example.myfactoryapplication.network.RetrofitInstance;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private static final long refreshInMillis = 300000; //5 minutes = 300000 milliseconds
    RecyclerView recyclerView;
    NewsAdapter adapter;
    Disposable timer2, timer1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.main_recyclerView);
        recyclerView.setAdapter(new NewsAdapter(new ArrayList<>()));
        recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));

        setupToolbar();
        initializeUI();
    }

    private void initializeUI() {
        if (!firstLoad()) {
            return;
        }

        timer1 = Observable.interval(refreshInMillis, TimeUnit.MILLISECONDS) //can be unsubscribed later
                .timeInterval()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(longTimed -> {
                    fetchData();
                });
    }

    private boolean firstLoad() {
        long timeDiff = System.currentTimeMillis() - LocalStoreManager.getLastFetchTime(this);

        if (timeDiff > refreshInMillis || LocalStoreManager.loadNewsLocally(this).isEmpty()) {
            fetchData();

            return true;
        } else {
            generateNewsList(LocalStoreManager.loadNewsLocally(this));

            timer2 = Observable.interval(1L, TimeUnit.MILLISECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .takeUntil(aLong -> aLong == refreshInMillis - timeDiff - 1)
                    .doOnComplete(this::initializeUI
                    ).subscribe();

            return false;
        }
    }

    private void fetchData() {
        Log.e("Test", "FETCH");

        GetNewsDataService service = RetrofitInstance.getRetrofitInstance().create(GetNewsDataService.class);
        Call<NewsList> call = service.getNewsData("bbc-news", "top", "6946d0c07a1c4555a4186bfcade76398");

        findViewById(R.id.main_progressBar).setVisibility(View.VISIBLE);
        call.enqueue(new Callback<NewsList>() {
            @Override
            public void onResponse(@NonNull Call<NewsList> call, @NonNull Response<NewsList> response) {
                findViewById(R.id.main_progressBar).setVisibility(View.GONE);

                if (response.code() != 200) {
                    CustomAlerts.showError(MainActivity.this, "database");
                    Log.e("response code", String.valueOf(response.code()));
                    return;
                }

                assert response.body() != null;
                generateNewsList(response.body().getNewsArrayList());
                LocalStoreManager.saveNewsLocally(MainActivity.this, response.body().getNewsArrayList());
            }

            @Override
            public void onFailure(@NonNull Call<NewsList> call, @NonNull Throwable t) {
                findViewById(R.id.main_progressBar).setVisibility(View.GONE);

                CustomAlerts.showError(MainActivity.this, "unknown");
            }
        });
    }

    private void generateNewsList(ArrayList<News> noticeArrayList) {
        Log.e("Test", "DISPLAY");

        recyclerView = findViewById(R.id.main_recyclerView);
        adapter = new NewsAdapter(noticeArrayList);
        adapter.notifyItemRangeChanged(adapter.getItemCount(), noticeArrayList.size());
        recyclerView.setAdapter(adapter);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerView.setLayoutManager(layoutManager);
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.main_toolbar);
        toolbar.setTitle("Factory News");
        setSupportActionBar(toolbar);
    }
}
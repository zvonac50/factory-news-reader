package com.example.myfactoryapplication.presenters

import com.example.myfactoryapplication.ClientData
import com.example.myfactoryapplication.MyFactoryApplication
import com.example.myfactoryapplication.models.obj.News
import com.example.myfactoryapplication.models.resp.NewsResp
import com.example.myfactoryapplication.network.ApiInterface
import com.example.myfactoryapplication.system.StorageUtil
import com.example.myfactoryapplication.ui.news_dashboard.DashboardInterface
import retrofit2.Call
import retrofit2.Response
import javax.security.auth.callback.Callback

class DashboardPresenter(val view: DashboardInterface) {
    fun getData() {
        val data: ArrayList<News> = arrayListOf()
        view.showLoading()

        ApiInterface.createAPI().newsList("bbc-news", "top", ClientData.API_KEY).enqueue(object : Callback, retrofit2.Callback<NewsResp> {
            override fun onResponse(call: Call<NewsResp>, response: Response<NewsResp>) {
                view.hideLoading()

                if (response.isSuccessful) {
                    if (response.body()!!.newsList.size == 0) {
                        view.showError("Data Not Yet Available")
                    } else {
                        for (item in response.body()!!.newsList) {
                            data.add(item)
                        }

                        view.showResult(data)
                        StorageUtil(MyFactoryApplication.ApplicationContext).saveNewsLocally(data)
                    }
                } else {
                    view.showError("Problem With Reaching Server")
                }
            }

            override fun onFailure(call: Call<NewsResp>, t: Throwable) {
                view.hideLoading()
                view.showError("Connection Time Out")
            }
        })
    }
}
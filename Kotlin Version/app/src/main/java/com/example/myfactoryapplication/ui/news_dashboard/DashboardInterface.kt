package com.example.myfactoryapplication.ui.news_dashboard

interface DashboardInterface {
    fun showLoading()
    fun hideLoading()
    fun showError(msg: String)
    fun showResult(result: Any)
}